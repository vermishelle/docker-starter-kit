#!/bin/bash

touch tmp/restart.txt
docker-compose stop
docker-compose rm -vf
docker-compose -f docker-compose.yml -f docker-compose.dev.yml up -d --build
