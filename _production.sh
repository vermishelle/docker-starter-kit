#!/bin/bash

rm -rf tmp/restart.txt
docker-compose stop
docker-compose rm -f
docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d --build
