FROM 111h/alpine-passenger-standalone
ARG PACKAGES="postgresql-client pcre-dev"
RUN apk add --no-cache $PACKAGES
RUN gem install bundler
WORKDIR /usr/src/app
COPY Gemfile* /usr/src/app/
RUN bundle
