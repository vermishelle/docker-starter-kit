require 'sinatra/base'

class ExampleApp < Sinatra::Base
  get '/' do
    'Hello world!'
  end
end
